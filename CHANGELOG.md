# Changelog

### Version 1.1.1
Added support to read partition names which contain special characters

### Version 1.1.0
Added support for kernel 4.18+  
Set minimum calculation interval to 0.1s

### Version 1.0.1
Replaced tempfile command with mktemp

### Version 1.0.0
Initial release
